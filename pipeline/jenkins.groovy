pipeline {
    agent any
    
    parameters {
        string(name: 'ENVIRONMENT', defaultValue: 'dev', description: 'Environment to deploy to')
        booleanParam(name: 'ENABLE_TESTS', defaultValue: true, description: 'Enable running tests')
    }
    
    stages {
        stage('Clone') {
            steps {
                echo 'CLONE REPOSITORY'
                git branch: "${BRANCH}", url: "${REPO}"
            }
        }
        
        stage('Test') {
            when {
                expression { params.ENABLE_TESTS == true }
            }
            steps {
                echo 'TEST EXECUTION STARTED'
                sh 'make test'
            }
        }
        
        stage('Build') {
            steps {
                echo 'BUILD EXECUTION STARTED'
                sh 'make build'
            }
        }
        
        stage('Deploy') {
            steps {
                echo "DEPLOYING TO ${params.ENVIRONMENT}"
                sh "make push ENV=${params.ENVIRONMENT}"
            }
        }
    }
}
